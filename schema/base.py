#!/usr/bin/env python
# @Time    : 2020/4/2 17:04
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : base
# @Project : diagnosis-system
from extensions import ma
from marshmallow import fields


class BaseSchema(ma.ModelSchema):
    """
    提供一个MarshMellow的基础数据模型，用于简化其它数据模型的开发工作。
    """

    create_time = fields.Method('get_create_time')
    update_time = fields.Method('get_update_time')
    token = fields.Method('get_token')

    def get_create_time(self, obj):
        create_time = ''
        if obj and hasattr(obj, 'create_time') and obj.create_time:
            # 统一返回ISO8601表达的utc时间
            create_time = obj.create_time.strftime("%Y-%m-%dT%H:%M:%SZ")
        return create_time

    def get_update_time(self, obj):
        update_time = ''
        if obj and hasattr(obj, 'update_time') and obj.update_time:
            # 统一返回ISO8601表达的utc时间
            update_time = obj.update_time.strftime("%Y-%m-%dT%H:%M:%SZ")
        return update_time

    def get_token(self, obj):
        """
        TODO:以后再做token验证
        :return:
        """
        return 'admin-token'

