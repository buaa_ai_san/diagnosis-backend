#!/usr/bin/env python
# @Time    : 2020/4/3 11:15
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : user
# @Project : diagnosis-system
from marshmallow import fields

from .base import BaseSchema
from models import User, Role, Department


class UserSchema(BaseSchema):

    class Meta:
        model = User

    roles = fields.Method('get_roles')

    def get_roles(self, obj):
        return [role.name for role in obj.roles]


class RoleSchema(BaseSchema):

    class Meta:
        model = Role


class DepartmentSchema(BaseSchema):

    class Meta:
        model = Department
