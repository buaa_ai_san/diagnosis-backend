#!/usr/bin/env python
# @Time    : 2020/4/3 14:08
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : image
# @Project : diagnosis-system
from .base import BaseSchema
from models import Image


class ImageSchema(BaseSchema):

    class Meta:
        model = Image
