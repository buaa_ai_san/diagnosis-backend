#!/usr/bin/env python
# @Time    : 2020/4/3 12:07
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : image
# @Project : diagnosis-system
from config import ImageCategory
from extensions import db
from models.base import BaseModel


class Image(BaseModel):
    """角色表  表名role"""
    __tablename__ = 'image'

    url = db.Column(db.String(300), comment='图片链接')
    name = db.Column(db.String(300), comment='图片名称')
    done = db.Column(db.Boolean, comment='诊断是否已完成')
    category = db.Column(db.Enum(
        ImageCategory.AK, ImageCategory.BCC, ImageCategory.BKL,
        ImageCategory.DF, ImageCategory.MEL, ImageCategory.NV,
        ImageCategory.SCC, ImageCategory.VASC, ImageCategory.UNK),
        comment='组织类型')
    possibility = db.Column(db.Float, comment='真阳性概率')

    def __repr__(self):
        return "<Db.Model.Image:%r>" % self.id