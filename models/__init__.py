#!/usr/bin/env python
# @Time    : 2020/4/2 15:17
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : __init__.py
# @Project : diagnosis-system
from .user import User, Role, Department
from .image import Image
