#!/usr/bin/env python
# @Time    : 2020/4/2 16:18
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : user
# @Project : diagnosis-system
import datetime

from extensions import db
from flask_security import UserMixin, current_user
from flask_security import RoleMixin

from models.base import BaseModel


# 用户信息表与角色信息表为多对多关系，用此辅助表关联
roles_users = db.Table('roles_users',
                       db.Column('id', db.Integer, primary_key=True, autoincrement=True, comment='用户角色编号'),
                       db.Column('user_id', db.Integer, db.ForeignKey('user.id'), comment='用户编号'),
                       db.Column('role_id', db.Integer, db.ForeignKey('role.id'), comment='角色编号'),
                       db.Column('creator', db.String(20), comment='创建人'),
                       db.Column('create_time', db.DateTime, default=datetime.datetime.now, comment='创建时间'),
                       db.Column('update_time', db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='更新时间'),
                       db.Column('description', db.Text, comment='描述信息'))


DEFAULT_AVATAR = r'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'


class User(BaseModel, UserMixin):
    """用户表  表名user"""
    __tablename__ = 'user'

    username = db.Column(
        db.String(150), index=True, nullable=False, comment='用户名')  #: 用户名
    password = db.Column(db.String(128), comment='密码')  #: 密码
    email = db.Column(db.String(150), comment='邮箱')  #: 邮箱
    active = db.Column(db.Boolean(), default=True, comment='激活状态')  #: 激活状态
    identity_number = db.Column(db.String(20), comment='身份证号')  # 身份证
    gender = db.Column(db.Enum('男', '女'), comment='性别')
    roles = db.relationship('Role', secondary='roles_users',
                            backref=db.backref('users', lazy='dynamic'))
    department_id = db.Column(
        db.Integer, db.ForeignKey('department.id'), comment='科室')
    avatar = db.Column(db.String(300), default=DEFAULT_AVATAR, comment='头像链接')

    # user_login, AttributeError: 'User' object has no attribute 'current_login_at'
    current_login_at = db.Column(db.DateTime, comment='本次登录时间')
    last_login_at = db.Column(db.DateTime)
    last_login_ip = db.Column(db.String(100))
    current_login_ip = db.Column(db.String(100))
    login_count = db.Column(db.Integer)

    def __repr__(self):
        return "<Db.Model.User:%r>" % self.name


class Role(BaseModel, RoleMixin):
    """角色表  表名role"""
    __tablename__ = 'role'

    name = db.Column(
        db.Enum('admin', 'register', 'radio', 'onco'),
        unique=True, nullable=False, comment='角色名称')  #: 角色名

    def __repr__(self):
        return "<Db.Model.Role:%r>" % self.name


class Department(BaseModel):
    """科室信息表"""
    __tablename__ = 'department'

    name = db.Column(db.String(20), unique=True, nullable=False, comment='科室名称')
    hospitalization_uuid = db.Column(
        db.String(50), unique=True, comment='入院编号')
    users = db.relationship('User', backref='department', lazy='dynamic')

    def __repr__(self):
        return "<Db.Model.Department:%r>" % self.name
