#!/usr/bin/env python
# @Time    : 2020/4/2 17:04
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : base
# @Project : diagnosis-system
from datetime import datetime
from extensions import db


class BaseModel(db.Model):
    """
    所有数据模型的公共基类
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, comment='id')  #: 记录的唯一标识和主键
    creator = db.Column(db.String(50), comment='创建人')                            #: 创建这条记录的用户
    create_time = db.Column(db.DateTime, default=datetime.utcnow, comment='创建时间')  #: 创建记录的时间
    update_time = db.Column(db.DateTime, default=datetime.utcnow,
                            onupdate=datetime.utcnow, comment='更新时间')              #: 更新记录的时间
    description = db.Column(db.Text, comment='描述信息')                            #: 这条记录的描述信息

    def save(self):
        """
        保存当前模型的实例，适合小数据量的新建操作，或者数据更新操作，如果
        是批量保存大量数据，不要使用此方法。
        """
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """
        从数据库删除当前模型所对应的数据记录。
        """
        db.session.delete(self)
        db.session.commit()

    @property
    def create_time_str(self):
        return self.create_time.strftime("%Y-%m-%dT%H:%M:%SZ")

    @property
    def update_time_str(self):
        return self.update_time.strftime("%Y-%m-%dT%H:%M:%SZ")
