#!/usr/bin/env python
# @Time    : 2020/4/2 15:18
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : config
# @Project : diagnosis-system

import os
from datetime import timedelta
from utils import marshmallow_conf
from redis import Redis

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
marshmallow_conf.apply()


class ImageCategory:
    AK = 'AK'
    BCC = 'BCC'
    BKL = 'BKL'
    DF = 'DF'
    MEL = 'MEL'
    NV = 'NV'
    SCC = 'SCC'
    VASC = 'VASC'
    UNK = 'UNK'


class Config:
    DEBUG = True

    SECRET_KEY = "vNeFddrSwRjqDTnqSuFZy05TQHgEMm6o"
    SESSION_TYPE = 'redis'
    SESSION_REDIS = Redis(host='127.0.0.1', port='6379', db=1)

    # 登录过期时间
    PERMANENT_SESSION_LIFETIME = timedelta(hours=24)

    # flask_security 加密
    SECURITY_PASSWORD_SALT = 'fhasdgihwntlgy8f'
    # # 角色验证失败跳转url
    # SECURITY_UNAUTHORIZED_VIEW = lambda: '/api/v1/common/role_unauthorized'
    # # 未登录时跳转url
    # SECURITY_LOGIN_URL = '/api/v1/common/login_unauthorized'
    # 启用用户追踪，用于登录审计
    SECURITY_TRACKABLE = True

    # DEFAULT_LOGO_TEMPLATE = '/media/uploads/assets/img/def_icon.jpg'

    # limiter
    RATELIMIT_STORAGE_URL = 'redis://:@localhost:6379/2'

    # # 设置为False以禁用所有CSRF保护
    # WTF_CSRF_ENABLED = False

    # 使用CSRF保护扩展时，这可以控制每个视图是否受到默认保护。默认值为True
    # WTF_CSRF_CHECK_DEFAULT = False

    # sqlalchemy相关
    # 如果设置成 True (默认情况)，Flask-SQLAlchemy 将会追踪对象的修改并且发送信号
    SQLALCHEMY_TRACK_MODIFICATIONS = (not os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS"))  # Defaults True
    # SQLALCHEMY_POOL_SIZE = 2000
    SQLALCHEMY_ENGINE_OPTIONS = {'pool_size': 200,
                                 "pool_pre_ping": True,
                                 "pool_recycle": 300,
                                 "echo": False  # 调试时打开
                                 }

    # SQLALCHEMY_ECHO = True

    # 全局文件上传的大小限制
    MAX_CONTENT_LENGTH = 1024 * 1024 * 1024
    # flask-upload默认配置
    UPLOADS_DEFAULT_DEST = os.path.join(BASE_DIR, 'static/')

    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://root:root@127.0.0.1:3306/diagnosis'

    # 模型相关配置
    MODEL_PATH = os.path.join(BASE_DIR, 'static/model-b5.sim.onnx')
    CATEGORIES = {
        ImageCategory.AK: 'Actinic keratosis',
        ImageCategory.BCC: 'Basal cell carcinoma',
        ImageCategory.BKL: 'Benign keratosis',
        ImageCategory.DF: 'Dermatofibroma',
        ImageCategory.MEL: 'Melanoma',
        ImageCategory.NV: 'Melanocytic nevus',
        ImageCategory.SCC: 'Squamous cell carcinoma',
        ImageCategory.VASC: 'Vascular lesion',
        ImageCategory.UNK: 'Unknown'
    }
    IMG_SIZE = 456

    # 启动地址和端口
    # HOST = '127.0.0.1'
    # PORT = 5000
    HOST = '182.92.85.189'
    PORT = 9528




