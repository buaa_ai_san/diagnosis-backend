import os
import time

import numpy as np
import _thread
from config import BASE_DIR
from .InferCore import efficientnet, densenet, resnext


class InferFusion:

    ret_map = ['AK','BCC','BKL','DF','MEL','NV','SCC','VASC']

    def __init__(self):
        # 注意，此处第一个参数的数字表示输入图片的尺寸，请按照模型接受的实际大小修改
        self.efficient_core = efficientnet.Core(300, os.path.join(BASE_DIR, 'static/EfficientNet-B3.sim.onnx'))
        self.dense_core = densenet.Core(456, os.path.join(BASE_DIR, 'static/DenseNet.sim.onnx'))
        self.resnext_core = resnext.Core(456, os.path.join(BASE_DIR, 'static/resnext.onnx.sim.onnx'))

    def infer(self, input_image):
        # result = []
        # _thread.start_new_thread(
        #     self.efficient_core.get_result, (input_image, result))
        # _thread.start_new_thread(
        #     self.dense_core.get_result, (input_image, result))
        # _thread.start_new_thread(
        #     self.resnext_core.get_result, (input_image, result))
        # i = 0
        # while len(result) < 3 and i < 50:
        #     i += 1
        #     time.sleep(0.1)
        # return result
        return [
            self.efficient_core.get_result(input_image),
            self.dense_core.get_result(input_image),
            self.dense_core.get_result(input_image)
        ]

    def l1_fusion(self, results):
        '''
        直接求平均的融合
        '''
        sum_of_results = np.zeros(8)
        for result in results:
            sum_of_results += np.array(result['softmax'])
        fv = sum_of_results / len(results)
        idx = np.argmax(fv)
        if fv[idx] < 0.5:
            return {'result': 'UNK', 'prob': 0, 'softmax': [0] * 8}
        return {'result': self.ret_map[idx], 'prob': fv[idx], 'softmax': fv}

    def l1_gate_fusion(self, results):
        '''
        只用预测结果概率大于 0.5 的结果求平均的融合
        '''
        sum_of_results = np.zeros(8)
        count = 0
        for result in results:
            if result['prob'] < 0.5:
                continue
            sum_of_results += np.array(result['softmax'])
            count += 1
        if count == 0:
            return {'result': 'UNK', 'prob': 0, 'softmax': [0] * 8}
        fv = sum_of_results / count
        idx = np.argmax(fv)
        return {'result': self.ret_map[idx], 'prob': fv[idx], 'softmax': fv}

    def get_result(self, image_path):
        results = self.infer(image_path)
        print(results)
        result = self.l1_gate_fusion(results)
        return result

    def init_app(self, app):
        app.fusion = self

# core = InferFusion()
# results = core.infer('train/AK/ISIC_0024511.jpg')
# print(f'模型单独推理的结果： ')
# for i in results:
#     print(i)
#
# print(f'直接求平均的融合: {core.l1_fusion(results)}')
#
# print(f'只用预测结果概率大于 0.5 的结果求平均的融合: {core.l1_gate_fusion(results)}')
