#!/usr/bin/env python
# @Time    : 2020/4/2 15:20
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : marshmallow_conf
# @Project : diagnosis-system
from marshmallow import fields


def apply():
    #  marshmallow文字默认值
    fields.Field.default_error_messages = {
        'required': u'缺少必填数据.',
        'type': u'数据类型不合法.',
        'null': u'数据不能为空.',
        'validator_failed': u'非法数据.'
    }
    fields.Str.default_error_messages = {
        'invalid': '不是合法文本.'
    }
    fields.Int.default_error_messages = {
        'invalid': u'不是合法整数.'
    }
    fields.Number.default_error_messages = {
        'invalid': u'不是合法数字.'
    }
    fields.Boolean.default_error_messages = {
        'invalid': u'不是合法布尔值.'
    }
