import os
import numpy as np
from PIL import Image
import onnxruntime
from config import Config


class Core:

    img_size = Config.IMG_SIZE # 456 for B5, 224 for B0, 260 for B2
    model_path = Config.MODEL_PATH
    ret_map = Config.CATEGORIES.keys()

    def __init__(self):
        self._init_model()

    def _img_preprocess(self, image_path):
        try:
            img = Image.open(image_path).resize(
                (self.img_size, self.img_size), Image.BICUBIC).convert('RGB')
        except OSError:
            print(f'\nFile broken: {image_path}')
            return None
        input_data = np.array(img).transpose(2, 0, 1)
        # 预处理
        img_data = input_data.astype('float32')
        mean_vec = np.array([0.485, 0.456, 0.406])
        stddev_vec = np.array([0.229, 0.224, 0.225])
        norm_img_data = np.zeros(img_data.shape).astype('float32')
        for i in range(img_data.shape[0]):
            norm_img_data[i,:,:] = (img_data[i,:,:]/255 - mean_vec[i]) / stddev_vec[i]
        # add batch channel
        norm_img_data = norm_img_data.reshape(
            1, 3, self.img_size, self.img_size).astype('float32')
        return norm_img_data

    def _init_model(self):
        self.session = onnxruntime.InferenceSession(self.model_path, None)
        self.model_input = self.session.get_inputs()[0].name
        return self.session, self.model_input

    def get_result(self, image_path):

        def softmax(x):
            e_x = np.exp(x - np.max(x))
            return e_x / e_x.sum(axis=0)

        norm_img_data = self._img_preprocess(image_path)
        if norm_img_data is None:
            return None
        fv = self.session.run([], {self.model_input: norm_img_data})[0][0]
        idx = np. argmax(fv)
        prob = list(softmax(fv))
        return {'category': list(self.ret_map)[idx], 'prob': list(prob)[idx],
                'softmax': prob}
