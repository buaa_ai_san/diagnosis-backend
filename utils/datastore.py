#!/usr/bin/env python
# @Time    : 2020/4/2 16:27
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : datastore
# @Project : diagnosis-system
# -*- coding:utf-8 -*-
from flask_script import Command, Option
from flask_security import SQLAlchemyUserDatastore, Security


class UserDataStore(object):
    def __init__(self, app=None):
        if app:
            self.app = app
        self.sud = None

    def init_app(self, app):
        from extensions import db
        from models.user import User, Role
        self.sud = SQLAlchemyUserDatastore(db, User, Role)
        security = Security(app, self.sud)
        app.security_user_datastore = self.sud
        app.security = security


class SuperUser(Command):
    """
    创建超级管理员
    """
    option_list = (
        Option('-u', '--username', dest='username', required=True),
        Option('-p', '--password', dest='password', required=True),
    )

    def run(self, username=None, password=None):
        from extensions import db
        from models.user import User, Role
        user_datastore = SQLAlchemyUserDatastore(db, User, Role)
        users = db.session.query(User).filter(User.username == username).all()
        admin_role = user_datastore.find_or_create_role(
            name='admin', description='管理员')
        register_role = user_datastore.find_or_create_role(
            name='register', description='register')
        radio_role = user_datastore.find_or_create_role(
            name='radio', description='radio')
        db.session.commit()
        if not users:
            # 创建管理员
            admin = user_datastore.create_user(
                username=username, password=password,
                email="%s@qq.com" % username)
            user_datastore.add_role_to_user(admin, admin_role)
            db.session.commit()
            print('超级管理员【%s】创建成功！' % username)
        else:
            print('用户名【%s】已存在！' % username)


