from . import base
from PIL import Image
import numpy as np


# class Core(base.Core):
#
#     def get_result(self, image_path):
#
#         def softmax(x):
#             e_x = np.exp(x - np.max(x))
#             return e_x / e_x.sum(axis=0)
#
#         norm_img_data = self._img_preprocess(image_path)
#         if norm_img_data is None:
#             return None
#         fv = self.session.run([], {self.model_input: norm_img_data})[0][0][:8] # 模型有问题暂时这样补丁
#         idx = np.argmax(fv)
#         prob = list(softmax(fv))
#         return {'result': self.ret_map[idx], 'prob': prob[idx], 'softmax': prob}
class Core(base.Core):

    def _img_preprocess(self, image_path):
        try:
            img = self.get_img(image_path)
        except OSError:
            print(f'\nFile broken: {image_path}')
            return None
        input_data = np.array(img).transpose(2, 0, 1)
        # 预处理
        img_data = input_data.astype('float32')
        mean_vec = np.array([0.5071, 0.4867, 0.4408])
        stddev_vec = np.array([0.2765, 0.2565, 0.2761])
        norm_img_data = np.zeros(img_data.shape).astype('float32')
        for i in range(img_data.shape[0]):
            norm_img_data[i, :, :] = (img_data[i, :, :] / 255 - mean_vec[i]) / stddev_vec[i]
        # add batch channel
        norm_img_data = norm_img_data.reshape(1, 3, self.img_size, self.img_size).astype('float32')
        return norm_img_data

    def get_result(self, image_path):
        def softmax(x):
            e_x = np.exp(x - np.max(x))
            return e_x / e_x.sum(axis=0)
        norm_img_data = self._img_preprocess(image_path)
        if norm_img_data is None:
            return None
        fv = self.session.run([], {self.model_input: norm_img_data})[0][0]
        idx = np.argmax(fv)
        fv = list(softmax(fv))
        return {'result': self.ret_map[idx], 'prob': fv[idx], 'softmax': fv}
