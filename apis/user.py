#!/usr/bin/env python
# @Time    : 2020/4/2 18:02
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : user
# @Project : diagnosis-system
from flask import request
from flask_login import current_user
from flask_restplus import Namespace, Resource
from flask_security import login_user, logout_user

from models import User
from schema.user import UserSchema
from utils.security import login_required

ns_user = Namespace('user', description="用户接口")
ns_users = Namespace('users', description="用户接口")


@ns_users.route('')
class UsersApi(Resource):

    def get(self):
        return {'hello': 'world'}


@ns_user.route('')
class UserLoginApi(Resource):
    """用户登录"""

    @login_required
    def get(self):
        """登录者信息"""
        data = UserSchema().dump(current_user)
        return {'message': 'success', "code": 200, 'data': data}


@ns_user.route('/login')
class UserLoginApi(Resource):
    """用户登录"""

    @ns_user.param('password', description='密码', _in='formData')
    @ns_user.param('username', description='用户名', _in='formData')
    def post(self):
        """用户登录"""
        if current_user.is_authenticated:
            data = UserSchema().dump(current_user)
            return {'message': '该用户已经登录！', "code": 200, 'data': data}

        username = request.form.get('username', '')
        password = request.form.get('password', '')
        # TODO: 以后对密码加密保存
        user = User.query.filter(
            User.username == username, User.password == password).first()
        if not user:
            return {'message': '用户名或密码错误！', "code": 400}
        login_user(user)
        data = UserSchema().dump(user)
        return {'message': '登录成功！', "code": 200, 'data': data}


@ns_user.route('/logout')
class UserLogoutApi(Resource):
    """用户注销管理"""

    def post(self):
        """
        处理用户注销请求
        :return:
        """
        logout_user()
        return {'message': '用户注销成功！', 'code': 200, 'data': 'success'}
