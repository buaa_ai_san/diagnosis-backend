#!/usr/bin/env python
# @Time    : 2020/4/2 15:16
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : __init__.py
# @Project : diagnosis-system
from flask import Blueprint
from flask_restplus import Api

from .user import ns_user
from .image import ns_image

bp_api = Blueprint('api', __name__)
api_resources = Api(bp_api, version='v1')
api_resources.add_namespace(ns_user)
api_resources.add_namespace(ns_image)
