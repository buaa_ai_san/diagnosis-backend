#!/usr/bin/env python
# @Time    : 2020/4/3 14:19
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : image
# @Project : diagnosis-system
import time
from uuid import uuid1
import os.path as osp
from os import remove
import base64

from flask import request
from flask_restplus import Namespace, Resource
from marshmallow import ValidationError
from werkzeug.utils import secure_filename

from models import Image
from schema.image import ImageSchema
from extensions import upload_image
# from utils.fusion import InferFusion
from flask import current_app
from utils.security import login_required
# from utils.infer import Core
from config import Config

ns_image = Namespace('images', description="图片接口")


class ImageResource(Resource):
    """
    图片上传基类resource
    """

    @staticmethod
    def gen_name(suffix):
        """生成基于uuid的文件新名称"""
        name = '%s.%s' % (str(uuid1()), suffix)
        return name

    def remove_old_file(self, file_path):
        """删除旧文件"""
        path = self.upload_obj.path(osp.basename(file_path))
        if osp.isfile(path):
            remove(path)

    def save(self, file_obj, path_type='url'):
        """
        用于处理附件上传
        :param path_type: 返回的路径类型(文件绝对路径/url样式路径)，默认为文件绝对路径
        :param file_obj: 从前端获取的文件对象
        :return: 文件保存路径
        """
        try:
            scu_file = secure_filename(file_obj.filename)
            suffix = scu_file.split('.')[-1]
            new_name = self.gen_name(suffix)
            upload_image.save(file_obj, name=new_name)
            if path_type == 'url':
                file_path = upload_image.url(new_name)
            else:
                file_path = upload_image.path(new_name)
            return file_path
        except Exception as e:
            return None

    def save_base64(self, base64_str, suffix):
        new_name = self.gen_name(suffix)
        image_url = f'http://{Config.HOST}:{Config.PORT}/static/images/{new_name}'
        image_path = f'{Config.UPLOADS_DEFAULT_DEST}/images/{new_name}'
        with open(image_path, 'wb') as f:
            f.write(base64.b64decode(base64_str))
        return image_url, image_path


@ns_image.route('')
class ImagesApi(ImageResource):
    """图片接口"""

    @login_required
    def get(self):
        """
        图片列表，分页
        :return:
        """
        images = Image.query.order_by(Image.create_time.desc()).all()
        data = ImageSchema(many=True).dump(images)
        return {'message': '请求成功', "code": 200, 'data': data}

    @ns_image.param('image', decorators='图片', _in='formData')
    @ns_image.param('name', decorators='图片名称', _in='formData')
    @login_required
    def post(self):
        """
        上传图像
        :return:
        """
        image = request.form.get('image')
        name = request.form.get('name')
        suffix = name.split('.')[-1]
        name = name.split('.')[0]
        now = time.time()
        image_url, image_path = self.save_base64(image, suffix)
        now1 = time.time()
        print(f'{now1-now}')
        # result = InferFusion().get_result(image_path)
        # result = InferFusion().get_result(image)
        result = current_app.fusion.get_result(image_path)
        now2 = time.time()
        print(f'{now2-now}')
        print(result)
        if not image_url or not result:
            return {'message': '保存图片出错或识别出错', "code": 400}
        data = {'url': image_url, 'name': name, 'category': result['result'],
                'done': True, 'possibility': result['prob']}
        try:
            image = ImageSchema().load(data)
            image.save()
            data = ImageSchema().dump(image)
            return {'message': '上传成功', "code": 200, 'data': data}
        except ValidationError as err:
            return {'message': '保存图片出错', "code": 400}


@ns_image.route('/<int:image_id>')
class ImagesApi(ImageResource):
    """删除图片"""

    def delete(self, image_id):
        """删除图片"""
        image = Image.query.get(image_id)
        url = image.url
        name = url.split('/')[-1]
        image.delete()
        path = f'{Config.UPLOADS_DEFAULT_DEST}/images/{name}'
        if osp.isfile(path):
            remove(path)
        return {'message': '移除成功', "code": 200}
