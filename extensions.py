#!/usr/bin/env python
# @Time    : 2020/4/2 15:18
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : extensions
# @Project : diagnosis-system
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, configure_uploads, patch_request_class
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate

from utils.datastore import UserDataStore
from utils.fusion import InferFusion

user_datastore = UserDataStore()
db = SQLAlchemy()
ma = Marshmallow()
# api访问限频配置
limiter = Limiter(key_func=get_remote_address)
IMAGES = tuple('jpg jpeg png gif'.split())
upload_image = UploadSet(name='images', extensions=IMAGES)
upload_list = [upload_image]
api = Api()
migrate = Migrate()
fusion = InferFusion()


def init_extension(app):
    db.init_app(app)
    api.init_app(app)
    migrate.init_app(app, db=db)

    ma.init_app(app)

    limiter.init_app(app)

    # 上传的初始化
    configure_uploads(app, upload_list)
    # flask-uploads 设置上传文件大小，默认64M，设置为None，大小由MAX_CONTENT_LENGTH决定
    patch_request_class(app, size=None)

    # 用户角色初始化app
    user_datastore.init_app(app)

    # 配置api访问限频
    limiter.init_app(app)
    fusion.init_app(app)
