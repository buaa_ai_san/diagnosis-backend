#!/usr/bin/env python
# @Time    : 2020/4/2 16:45
# @Author  : 洪英杰
# @Python  : 3.7.5
# @File    : manage
# @Project : diagnosis-system

import config
import os
from flask import Flask
from flask_migrate import MigrateCommand
from flask_script import Manager

from apis import bp_api
from extensions import init_extension, db
from utils.datastore import SuperUser
from config import Config

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def create_app():
    # 应用各种配置
    app = Flask(__name__, static_folder='static')
    app.config.from_object(config.Config)
    app.register_blueprint(bp_api, url_prefix='/api')
    print(app.url_map)
    db.app = app
    init_extension(app=app)
    manager = Manager(app=app)
    manager.add_command('db', MigrateCommand)
    manager.add_command('superuser', SuperUser)
    return app, manager


this_app, this_manager = create_app()


if __name__ == '__main__':
    this_manager.run()
    # this_app.run(port=Config.PORT, host=Config.HOST)
